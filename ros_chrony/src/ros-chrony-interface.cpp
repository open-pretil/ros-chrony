#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <vector>
#include "clock_sync_gps/ClockInfo.h" 
#include "clock_sync_gps/ClockList.h"

int main( int argc, char *argv[] )
{

  int freq;

  ros::init(argc, argv, "ros_chrony_interface");

  ros::NodeHandle nh;

  ros::param::param<int>("~freq", freq, 5);

  ros::Publisher pubGPSClock = nh.advertise<clock_sync_gps::ClockInfo>("/clock_gps", 1000);
  ros::Publisher pubPPSClock = nh.advertise<clock_sync_gps::ClockInfo>("/clock_pps", 1000); 
  ros::Publisher pubListClock = nh.advertise<clock_sync_gps::ClockList>("/clock_list", 1000);

  ROS_INFO("RATE IS : %d", freq);

  ros::Rate rate(freq);

  FILE *fp;
  char path[1035];
  std::string gps_;
  std::string pps_;
  std::string mode_, state_;

  int pos_sign_1, pos_sign_2, pos_sign_3, pos_state, pos_name;
  int pos_sign_1_pps, pos_sign_2_pps, pos_sign_3_pps;

  clock_sync_gps::ClockInfo msgClockInfo_gps = clock_sync_gps::ClockInfo();
  clock_sync_gps::ClockInfo msgClockInfo_pps = clock_sync_gps::ClockInfo();

  clock_sync_gps::ClockList msgClockList = clock_sync_gps::ClockList();

  ros::Duration t1_gps(0);
  ros::Duration t2_gps(0);
  ros::Duration t3_gps(0);

  ros::Duration t1_pps(0);
  ros::Duration t2_pps(0);
  ros::Duration t3_pps(0);

  while(ros::ok()){
      
    // Open the command for reading.
    fp = popen("chronyc sources | grep ^#" , "r");
    if (fp == NULL) {
      printf("Failed to run command\n" );
      exit(1);
    }

    // Read the output a line at a time - output it.
    while (fgets(path, sizeof(path), fp) != NULL) {
    
      if(boost::algorithm::contains(path, "GPS")){

        gps_ = path;

      }else if(boost::algorithm::contains(path, "PPS")){

        pps_ = path; 
      }
      
    }


    boost::algorithm::erase_all(gps_, " ");
    boost::algorithm::erase_all(pps_, " ");
    
    //building msg for GPS

    pos_sign_1 = gps_.find_first_of("+-", 2); // ignoring start of string #- or #+ to get first measure
    pos_sign_2 = gps_.find_first_of("+-", pos_sign_1+1); // from 1st sign detected find next
    pos_sign_3 = gps_.find_first_of("/"); 

    t1_gps.nsec = std::stoi(gps_.substr(pos_sign_1, (pos_sign_2-pos_sign_1)-1));
    t2_gps.nsec = std::stoi(gps_.substr(pos_sign_2, (pos_sign_3 - pos_sign_2)-1));
    t3_gps.nsec = std::stoi(gps_.substr(pos_sign_3+2, gps_.find('\n')-pos_sign_3-2)); //

    switch(gps_.at(0)){

        case '#':
             msgClockInfo_gps.mode = "Local Clock";
             break;
        
        case '^':
             msgClockInfo_gps.mode = "Server";
             break;

        case '=':
             msgClockInfo_gps.mode = "Peer";
             break;

        default:
            msgClockInfo_gps.mode = "Error while retreiving mode";
            break;
    }

    switch(gps_.at(1)){

        case '*':
             msgClockInfo_gps.state = "Current best";
             break;
        
        case '+':
             msgClockInfo_gps.state = "Combined";
             break;

        case '-':
             msgClockInfo_gps.state = "Not Combined";
             break;

        case 'x':
             msgClockInfo_gps.state = "May be in error";
             break;

        case '~':
             msgClockInfo_gps.state = "Too variable";
             break;

        case '?':
             msgClockInfo_gps.state = "Unusable";
             break;

        default:
            msgClockInfo_gps.state = "Error while retreiving mode";
            break;
    }


    msgClockInfo_gps.name = gps_.substr(2,3);
    //msgClockInfo_gps.mode = gps_.substr(0,1); -> switch case
    //msgClockInfo_gps.state = gps_.substr(1,1); -> switch case
    msgClockInfo_gps.adjusted_offset = t1_gps; // substring to send data from 1st sign 
    msgClockInfo_gps.measured_offset = t2_gps;  // substring to send data from 2nd sign  
    msgClockInfo_gps.estimated_error = t3_gps;  // substring to send data from last sign
  
  
    //Building msg for PPS
    pos_sign_1_pps = pps_.find_first_of("+-", 2); // ignoring start of string #- or #+ to get first measure
    pos_sign_2_pps = pps_.find_first_of("+-", pos_sign_1_pps+1); // from 1st sign detected find next
    pos_sign_3_pps = pps_.find_first_of("/"); // from 2nd sign detected find next (last)

    t1_pps.nsec = std::stoi(pps_.substr(pos_sign_1_pps, (pos_sign_2_pps-pos_sign_1_pps)-1));
    t2_pps.nsec = std::stoi(pps_.substr(pos_sign_2_pps, (pos_sign_3_pps - pos_sign_2_pps)-1));
    t3_pps.nsec = std::stoi(pps_.substr(pos_sign_3_pps+2, pps_.find('\n')-pos_sign_3_pps-2)); // errr ?


    switch(pps_.at(0)){

        case '#':
             msgClockInfo_pps.mode = "Local Clock";
             break;
        
        case '^':
             msgClockInfo_pps.mode = "Server";
             break;

        case '=':
             msgClockInfo_pps.mode = "Peer";
             break;

        default:
            msgClockInfo_pps.mode = "Error while retreiving mode";
            break;
    }

    switch(pps_.at(1)){

        case '*':
             msgClockInfo_pps.state = "Current best";
             break;
        
        case '+':
             msgClockInfo_pps.state = "Combined";
             break;

        case '-':
             msgClockInfo_pps.state = "Not Combined";
             break;

        case 'x':
             msgClockInfo_pps.state = "May be in error";
             break;

        case '~':
             msgClockInfo_pps.state = "Too variable";
             break;

        case '?':
             msgClockInfo_pps.state = "Unusable";
             break;

        default:
            msgClockInfo_pps.state = "Error while retreiving mode";
            break;
    }

    msgClockInfo_pps.name = pps_.substr(2,3);
    //msgClockInfo_pps.mode = pps_.substr(0,1); -> switch case
    //msgClockInfo_pps.state = pps_.substr(1,1); -> switch case
    msgClockInfo_pps.adjusted_offset = t1_pps; // substring to send data from 1st sign 
    msgClockInfo_pps.measured_offset = t2_pps; // substring to send data from 2nd sign  
    msgClockInfo_pps.estimated_error = t3_pps; // substring to send data from last sign
  


    msgClockList.list = {msgClockInfo_gps, msgClockInfo_pps};
    msgClockList.raw_data = gps_ + pps_;

  //  std::cout << t1_gps.nsec << " " << t2_gps.nsec << " " << t3_gps.nsec << std::endl; 
   // std::cout << t1_pps.nsec << " " << t2_pps.nsec << " " << t3_pps.nsec << std::endl;

    pubGPSClock.publish(msgClockInfo_gps);
    pubPPSClock.publish(msgClockInfo_pps);
    pubListClock.publish(msgClockList);

    // close file
    pclose(fp);

 
    rate.sleep();
  }

  return 0;
}