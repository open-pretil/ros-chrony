# ros-chrony

ROS package that interfaces the time synchronization chrony daemon and provide status of synchronization on ROS topics.

## GPSD and Chrony Synchronization

### Installation of  GPSD
1. Download the lastest version of GPS [here](http://download-mirror.savannah.gnu.org/releases/gpsd/) -> choose at least v3.22 because lower versions can give you bugs according to official doc.
2. untar the file with `tar -xzf gpsd-XX.Y.tar.gz`
3. go to the gpsd directory with `cd gpsd-XX.Y`
4. go in root mode with `sudo su` to avoid any permissions problems
5. do `scons && scons check && scons udev-install` (you may need to install scons before `sudo apt install scons`)
6. This should work and you can start the daemon with gpsd to see if it is installed successfully.
7.  Official tutorial [here](https://gpsd.gitlab.io/gpsd/building.html) if you got any problems.

need to modify python version to install GPSD (TODO : give how later)

### Installation of Chrony

1. Download lastest release of chrony [here](https://chrony.tuxfamily.org/download.html)
2. Extract the file.
3. Go to the directory `cd chrony-X.Y`
4. To avoid any permissions problems go in root mode `sudo su`
5. Do `./configure` and wait until the end.
6. Once step 6 is done, do `make` to build programs
7. After step 6 do `make docs` to have HTML docs.
8. `make install` to install compiled programs of step 6.
9. `make install-docs` to install docs.
10. You should be able to run `chronyd` in a terminal.

### Configuring GPSD and Chrony

#### GPSD
For GPSD you can have a port listening problem, you maybe have to create a file in `/etc/systemd/system/gpsd.socket.d/` -> create a file named socket.conf

configure with this :

    [Socket]
    #First blank ListenStream clears the system defaults
    ListenStream=
    ListenStream=2947
    ListenStream=/var/run/gpsd.sock


#### Chrony
To synchronize GPS with chrony we need to modify this file `nano /etc/chrony/chrony.conf`.
In this file we have to add those lines at the end :
`
refclock SOCK /var/run/chrony.ttyS0.sock delay 0.0 refid GPS
refclock SHM 1 offset 0.0 delay 0.0 refid PPS
`

ttyS0 was our serial port you may be have to change that.

You can try to follow this tutorial [here](https://gpsd.gitlab.io/gpsd/gpsd-time-service-howto.html#_feeding_chrony_from_gpsd) this is official GPSD documentation, but the `refclock SOCK /run/chrony.ppsX.sock` didn't work for us.

After this you can start both daemons with

`sudo chronyd -f /etc/chrony/chrony.conf`
`sudo gpsd -n /dev/ttyS0` -> S0 is 1st serial port, you may need to change.

To check if it's working do : `chronyc sources -v` (-v is useful to understand symbols)

You should have something like : `#- GPS or #+ GPS` and under `#* PPS`
If you have those sign, sources are synchronized.

## other and tips

You have to change your GPS configuration to emit pps
(TODO : ADD how on ublox later)

see sync of chrony :
```
chronyc tracking
```
see gps data :
```
gpsd -n /dev/tty...
ntpd -gN
cgps
```
rerun services :
```
sudo systemctl restart chrony
sudo systemctl restart gpsd
```
PS :

make sure to uninstall ntp (ntp install will uninstall chrony)

lib use :

* gpsd
* ntpd
* cgps
* chrony

## Authors and acknowledgment

Corentin Boulongne & Gérald Dherbomez

## License

MIT License
